##
# Copyright (C) 2018-2021. Huawei Technologies Co., Ltd. All rights reserved.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##

# Python library for automation scripts

import argparse
import getpass
import hashlib
import json
import os
import re
import select
import shutil
import sys
import time
import uuid

import hdfs
import requests

_JSON_DEFAULT_OUTPUT = '-'
MAX_RETRY_COUNT = 10
RETRY_INTERVAL = 15


def get_json_data(json_obj, *keys):
    data = json_obj
    try:
        for key in keys:
            data = data[key]
    except:
        return _JSON_DEFAULT_OUTPUT

    if data is None:
        return _JSON_DEFAULT_OUTPUT

    return data


def copy_files_in_folder(src_dir, dest_dir):
    '''
    Copies files in src_dir to dest_dir. 
    Intended to be used in place of shutil.copytree(..., dirs_exist_ok=True); the dirs_exist_ok 
    parameter is only available in Python 3.8+.
    '''
    for root, dirnames, filenames in os.walk(src_dir):
        for dirname in dirnames:
            dir_path = os.path.join(root, dirname).replace(src_dir, dest_dir)
            if not os.path.exists(dir_path):
                os.makedirs(dir_path)

    for root, dirnames, filenames in os.walk(src_dir):
        for filename in filenames:
            src = os.path.join(root, filename)
            dest = src.replace(src_dir, dest_dir)
            shutil.copy(src, dest)


def display_cluster_status(arg_json_obj):
    print('--- openLooKeng Cluster Name: ' + get_json_data(arg_json_obj, 'name'))
    print('--- YARN app ID: ' + get_json_data(arg_json_obj, 'id'))

    components = get_json_data(arg_json_obj, "components")
    if components == _JSON_DEFAULT_OUTPUT:
        return

    print("--- {:<40} {:<15} {:<10} {:<25} {:<10} {:<20} {:<20} {:<10} {:<10} {:<10}"
          .format('Container ID', 'ComponentName', 'Instances', 'ComponentInstanceName', 'Status',
                  'Hostname', 'IP Addr', 'CPUs', 'Memory', 'Launch Time'))
    error_summary = ""
    for comp_index in range(len(components)):
        try:
            component = get_json_data(components, comp_index)
            if component == " - ":
                continue
            component_name = get_json_data(component, "name")
            instances = get_json_data(component, "number_of_containers")

            # For every instance
            resource = get_json_data(component, "resource")
            allocated_cpu_count = get_json_data(resource, "cpus")
            allocated_mem = get_json_data(resource, "memory")

            containers = get_json_data(component, "containers")
            containers.sort(key=lambda x: x["id"])
            for cont_index in range(len(containers)):
                try:
                    container = get_json_data(containers, cont_index)
                    if container == _JSON_DEFAULT_OUTPUT:
                        continue
                    component_id = get_json_data(container, "id")
                    component_instance_name = get_json_data(container, "component_instance_name")
                    status = get_json_data(container, "state")
                    hostname = get_json_data(container, "hostname")
                    ip_addr = get_json_data(container, "ip")
                    launch_time = get_json_data(container, "launch_time")

                    print("    {:<40} {:<15} {:<10} {:<25} {:<10} {:<20} {:<20} {:<10} {:<10} {:<10}"
                          .format(component_id, component_name, instances, component_instance_name, status,
                                  hostname, ip_addr, allocated_cpu_count, allocated_mem, launch_time))
                except Exception as e:
                    error_summary = error_summary + str(e)
        except Exception as e:
            error_summary = error_summary + str(e)
    print(error_summary)


def get_discovery_uri(seeds_json):
    try:
        arg_json_obj = json.loads(seeds_json)
    except ValueError as e:
        print(str(e))
        return None

    latest_timestamp = None
    latest_location = None
    for seed_index in range(len(arg_json_obj)):
        temp_seed = get_json_data(arg_json_obj, seed_index)
        timestamp = get_json_data(temp_seed, 'timestamp')
        if latest_timestamp is None or latest_timestamp < timestamp:
            latest_location = get_json_data(temp_seed, 'location')
            latest_timestamp = get_json_data(temp_seed, 'timestamp')

    return latest_location


def is_coordinator_stable(arg_json_obj):
    components = get_json_data(arg_json_obj, "components")
    if components == _JSON_DEFAULT_OUTPUT:
        return False

    for comp_index in range(len(components)):
        try:
            component = get_json_data(components, comp_index)
            if component == _JSON_DEFAULT_OUTPUT:
                continue
            component_name = get_json_data(component, "name")
            if component_name == _JSON_DEFAULT_OUTPUT or component_name != "coordinator":
                continue
            component_status = get_json_data(component, "state")
            if component_status == "STABLE":
                return True
        except:
            return False


def is_ha_enabled(config_params):
    '''
    Returns True if the given (existing) cluster has HA enabled, and False otherwise.
    The function will check this by checking for the existence of a (coordinator_)state-store.properties file in HDFS,
    which would have been uploaded if and only if HA was enabled in the original deployment.
    '''
    try:
        client = get_hdfs_client(config_params)
        client.status(config_params['hdfs-dir'] + '/conf/coordinator_etc/state-store.properties')

        return True
    except:
        return False


def is_nginx_deployed(config_params):
    '''
    Returns True if use-nginx was True in the original deployment. 
    The function will look in an olkonyarn_hdfs.properties file in HDFS to find it.
    '''
    client = get_hdfs_client(config_params)
    try:
        content = ""
        with client.read(config_params['hdfs-dir'] + '/conf/' + 'olkonyarn_hdfs.properties') as reader:
            content = str(reader.read(), 'utf-8')
        lines = content.split('\n')
        for line in lines:
            key_value = line.split('=')
            key = key_value[0]
            value = key_value[1]
            if key == 'use-nginx':
                return value == 'true'
        return False
    except Exception as e:
        print('--- Error while determining whether nginx was deployed: {0}'.format(str(e)))
        exit(1)


def is_nginx_endpoint_reported(config_params):
    '''
    Returns True if the nginx-endpoint-url.txt is in HDFS, which
    would have been used by nginx_container_entrypoint.py to report the nginx endpoint url.
    Return False if it does not exist.
    '''
    try:
        client = get_hdfs_client(config_params)
        client.status(config_params['hdfs-dir'] + '/nginx-endpoint-url.txt')
        return True
    except:
        return False


def yarnfile_add_olk_etc_configs(olk_json_template, config_file_names, add_to_component):
    with open(olk_json_template, 'r+') as jsonFile:
        json_obj = json.load(jsonFile)
        components = get_json_data(json_obj, "components")
        if components == _JSON_DEFAULT_OUTPUT:
            return

        for comp_index in range(len(components)):
            try:
                component = get_json_data(components, comp_index)
                if component == _JSON_DEFAULT_OUTPUT:
                    continue
                component_name = get_json_data(component, "name")
                if component_name == _JSON_DEFAULT_OUTPUT:
                    continue
                if component_name != add_to_component:
                    continue
                component_config = get_json_data(component, 'configuration')
                component_config_files = get_json_data(component_config, 'files')

                for filename in config_file_names:
                    idx = filename.rfind('/')
                    dest_filename = filename[idx + 1:]
                    dest_filename = dest_filename.replace(component_name + "_", '')
                    file_config = {
                        "type": "TEMPLATE",
                        "src_file": filename,
                        "dest_file": dest_filename
                    }
                    component_config_files.append(file_config)
                    json_obj['components'][comp_index]["configuration"]["files"] = component_config_files

            except Exception as e:
                print(str(e))

        jsonFile.seek(0)
        json.dump(json_obj, jsonFile, indent=4)
        jsonFile.truncate()


def get_cluster_name(cluster_info_json):
    name = get_json_data(cluster_info_json, "name")
    return name


def file_match_and_replace_string(filename, match_pattern, replace_pattern):
    with open(filename, "rt") as file:
        data = file.read()

    data = data.replace(str(match_pattern), str(replace_pattern))

    if data is not None:
        with open(filename, "wt") as file:
            file.write(data)


def stage_files(config_params):
    if os.path.exists(config_params['STAGEDIR']):
        shutil.rmtree(config_params['STAGEDIR'])
    try:
        os.mkdir(config_params['STAGEDIR'])
        os.mkdir(config_params['STAGEDIR'] + "/conf")
        os.mkdir(config_params['STAGEDIR'] + '/conf/coordinator_etc')
        os.mkdir(config_params['STAGEDIR'] + "/conf/worker_etc")
        os.mkdir(config_params['STAGEDIR'] + '/conf/nginx_etc')
        os.mkdir(config_params['STAGEDIR'] + "/bin")
    except OSError as error:
        print('Failed to create staging directory. Err: ' + str(error))
        exit(1)

    generate_olk_properties(config_params)

    static_files = {
        'bin/launcher.py',
        'bin/olk_container_entrypoint.py',
        'bin/nginx_container_entrypoint.py'
    }
    for file in static_files:
        src = config_params['WORKDIR'] + '/' + file
        dst = config_params['STAGEDIR'] + '/' + file
        shutil.copy(src, dst)

    # Copy template directories to stage

    coordinator_etc_template_path = config_params['WORKDIR'] + '/conf/etc_template/coordinator'
    worker_etc_template_path = config_params['WORKDIR'] + '/conf/etc_template/worker'
    coordinator_etc_stage = config_params['STAGEDIR'] + '/conf/coordinator_etc'
    worker_etc_stage = config_params['STAGEDIR'] + '/conf/worker_etc'

    copy_files_in_folder(coordinator_etc_template_path, coordinator_etc_stage)
    copy_files_in_folder(worker_etc_template_path, worker_etc_stage)

    if config_params['high-availability'] != 'true' and os.path.exists(coordinator_etc_stage + '/state-store.properties.template'):
        os.remove(coordinator_etc_stage + '/state-store.properties.template')
    if config_params['high-availability'] != 'true' and os.path.exists(worker_etc_stage + '/state-store.properties.template'):
        os.remove(worker_etc_stage + '/state-store.properties.template')
    
    shutil.copy(config_params['WORKDIR'] + '/conf/nginx.conf.template', config_params['STAGEDIR'] + '/conf/nginx.conf')

    if not os.path.exists(config_params['olk-yarnfile']):
        if not os.path.exists(config_params['WORKDIR'] + '/conf/' + config_params['olk-yarnfile']):
            print("Failed to locate " + config_params['olk-yarnfile'])
            exit(1)
        config_params['olk-yarnfile'] = config_params['WORKDIR'] + '/conf/' + config_params['olk-yarnfile']

    shutil.copy(config_params['olk-yarnfile'], config_params['STAGEDIR'] + "/conf/openlookeng.json")

    # Fill in some template strings, others will be filled in inside the container

    number_of_nginx = 0
    if config_params['use-nginx'] == 'true':
        number_of_nginx = 1

    for root, dirnames, filenames in os.walk(config_params['STAGEDIR'] + '/conf'):
        for filename in filenames:
            original_name = os.path.join(root, filename)
            new_name = original_name.replace('.template', '')
            os.rename(original_name, new_name)

    file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/openlookeng.json",
                                  '_OLK_JSON_TEMPLATE_WORKERS_COUNT', config_params['workers'])
    file_match_and_replace_string(config_params['STAGEDIR'] + '/conf/openlookeng.json',
                                  '_OLK_JSON_TEMPLATE_COORDINATORS_COUNT', config_params['coordinators'])
    file_match_and_replace_string(config_params['STAGEDIR'] + '/conf/openlookeng.json',
                                  '_OLK_JSON_TEMPLATE_NGINX_COUNT', number_of_nginx)
    file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/openlookeng.json",
                                  '_OLK_JSON_TEMPLATE_CLUSTER_NAME', config_params['cluster-name'])
    file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/openlookeng.json",
                                  '_OLK_JSON_TEMPLATE_ARTIFACTS_HDFS_PATH', config_params['hdfs-dir'])
    file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/openlookeng.json",
                                  '_OLK_JSON_TEMPLATE_COORDINATOR_CPUS', '1')
    file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/openlookeng.json",
                                  '_OLK_JSON_TEMPLATE_COORDINATOR_MEMORY', '1024')
    file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/openlookeng.json",
                                  '_OLK_JSON_TEMPLATE_WORKER_CPUS', '1')
    file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/openlookeng.json",
                                  '_OLK_JSON_TEMPLATE_WORKER_MEMORY', '512')

    file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/coordinator_etc/seed-store.properties",
                                  '_OLK_PROPERTIES_TEMPLATE_CLUSTER_NAME', config_params['cluster-name'])
    file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/worker_etc/seed-store.properties",
                                  '_OLK_PROPERTIES_TEMPLATE_CLUSTER_NAME', config_params['cluster-name'])
    shutil.copy(config_params['STAGEDIR'] + '/conf/worker_etc/seed-store.properties', config_params['STAGEDIR'] + '/conf/nginx_etc/seed-store.properties')

    file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/coordinator_etc/config.properties",
                                  '_OLK_PROPERTIES_TEMPLATE_HTTP_SERVER_PORT', config_params['olk-server-port'])
    file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/worker_etc/config.properties",
                                  '_OLK_PROPERTIES_TEMPLATE_HTTP_SERVER_PORT', config_params['olk-worker-port'])

    protocol = None
    if config_params['request-authentication'].lower() == 'insecure':
        protocol = 'http'
        file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/openlookeng.json",
                                      '_OLK_JSON_TEMPLATE_KERBEROS_PRINCIPAL_NAME', '')
        file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/openlookeng.json",
                                      '_OLK_JSON_TEMPLATE_KERBEROS_PRINCIPAL_KEYTAB', '')
    elif config_params['request-authentication'].lower() == 'kerberos':
        protocol = 'https'
        file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/openlookeng.json",
                                      '_OLK_JSON_TEMPLATE_KERBEROS_PRINCIPAL_NAME', config_params['kerberos-principal'])
        file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/openlookeng.json",
                                      '_OLK_JSON_TEMPLATE_KERBEROS_PRINCIPAL_KEYTAB', 'file://' + config_params['kerberos-keytab'])
    elif  config_params['request-authentication'].lower() == 'ssl':
        protocol = 'https'
        file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/openlookeng.json",
                                      '_OLK_JSON_TEMPLATE_KERBEROS_PRINCIPAL_NAME', '')
        file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/openlookeng.json",
                                      '_OLK_JSON_TEMPLATE_KERBEROS_PRINCIPAL_KEYTAB', '')

    file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/coordinator_etc/config.properties",
                                  '_OLK_PROPERTIES_TEMPLATE_DISCOVERY_URI',
                                  protocol + "://_OLK_PROPERTIES_TEMPLATE_CN_DISCOVERY_IP:" +
                                  str(config_params['olk-server-port']))

    if config_params['high-availability'] == "true":
        file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/coordinator_etc/state-store.properties",
                                      '_OLK_PROPERTIES_TEMPLATE_CLUSTER_NAME', config_params['cluster-name'])
        file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/worker_etc/state-store.properties",
                                      '_OLK_PROPERTIES_TEMPLATE_CLUSTER_NAME', config_params['cluster-name'])

        file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/coordinator_etc/config.properties",
                                      '_OLK_PROPERTIES_TEMPLATE_HA_ENABLED', 'true')
        file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/worker_etc/config.properties",
                                      '_OLK_PROPERTIES_TEMPLATE_HA_ENABLED', 'true')
    else:
        file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/coordinator_etc/config.properties",
                                      '_OLK_PROPERTIES_TEMPLATE_HA_ENABLED', 'false')
        file_match_and_replace_string(config_params['STAGEDIR'] + "/conf/worker_etc/config.properties",
                                      '_OLK_PROPERTIES_TEMPLATE_HA_ENABLED', 'false')

    # These olk config files need to be made available to the container before container entrypoint scripts can download anything from hdfs

    statically_localize_coordinator = [ config_params['hdfs-dir'] + '/conf/olkonyarn_hdfs.properties' ]
    statically_localize_worker = [ config_params['hdfs-dir'] + '/conf/olkonyarn_hdfs.properties' ]
    statically_localize_nginx = [ config_params['hdfs-dir'] + '/conf/olkonyarn_hdfs.properties', 
                                  config_params['hdfs-dir'] + '/conf/nginx_etc/seed-store.properties' ]
    
    yarnfile_add_olk_etc_configs(config_params['STAGEDIR'] + "/conf/openlookeng.json", statically_localize_coordinator, 'coordinator')
    yarnfile_add_olk_etc_configs(config_params['STAGEDIR'] + "/conf/openlookeng.json", statically_localize_worker, 'worker')
    yarnfile_add_olk_etc_configs(config_params['STAGEDIR'] + "/conf/openlookeng.json", statically_localize_nginx, 'nginx')


def get_olk_cluster(config_params):
    url = config_params['resource-manager-address'] + '/app/v1/services/' + config_params['cluster-name'] \
          + '?&user.name=' + config_params['yarn-user']
    response = None
    data = None
    if config_params['request-authentication'].lower() == 'insecure' or config_params['request-authentication'].lower() == 'ssl':
        response = requests.get(url)
    elif config_params['request-authentication'].lower() == 'kerberos':
        from requests_kerberos import HTTPKerberosAuth
        auth = HTTPKerberosAuth()
        response = requests.get(url, auth=auth)

    if response is not None:
        data = response.json()
        response.close()

    return data


def olk_cluster_exists(config_params):
    olk_cluster_info = get_olk_cluster(config_params)
    cluster_name = get_cluster_name(olk_cluster_info)
    if cluster_name == config_params['cluster-name']:
        return True

    return False


def get_hdfs_client(config_params):
    if config_params['request-authentication'].lower() == 'insecure' or config_params['request-authentication'].lower() == 'ssl':
        return hdfs.InsecureClient(config_params['name-node-address'], user=config_params['hdfs-user'])
    elif config_params['request-authentication'].lower() == 'kerberos':
        session = requests.Session()
        from hdfs.ext.kerberos import KerberosClient
        return KerberosClient(url=config_params['name-node-address'], session=session)

    print('--- Failed to retrieve hdfs client')
    exit(1)


def generate_olk_properties(config_params):
    filenames = [ config_params['STAGEDIR'] + '/conf/olkonyarn_hdfs.properties' ]
    configs = {
        'name-node-address': config_params['name-node-address'],
        'hdfs-user': config_params['hdfs-user'],
        'yarn-user': config_params['yarn-user'],
        'HDFSSEEDSTOREPATH': config_params['HDFSSEEDSTOREPATH'],
        'request-authentication': config_params['request-authentication'],
        'resource-manager-address': config_params['resource-manager-address'],
        'hdfs-dir': config_params['hdfs-dir'],
        'nginx-port': config_params['nginx-port'],
        'fail-if-nginx-port-in-use': config_params['fail-if-nginx-port-in-use'],
        'nginx-reload-check-interval': config_params['nginx-reload-check-interval'],
        'use-nginx': config_params['use-nginx']
    }
    for filename in filenames:
        with open(filename, 'w') as f:
            for key, value in configs.items():
                f.write('%s=%s\n' % (key, value))
                f.flush()


def cleanup_hdfs_olk_cluster(config_params):
    print('--- Cleanup required. Deleting remote hdfs directories: ')
    print('> ' + config_params['HDFSSEEDSTOREPATH'])
    print('> ' + config_params['hdfs-dir'])
    print('--- Press key in 20 seconds to abort...')
    i, o, e = select.select([sys.stdin], [], [], 20)
    if i:
        return

    client = get_hdfs_client(config_params)
    if client.content(config_params['HDFSSEEDSTOREPATH'], strict=False) is not None:
        if not client.delete(config_params['HDFSSEEDSTOREPATH'], recursive=True):
            print('--- Failed to delete ' + config_params['HDFSSEEDSTOREPATH'])

    if client.content(config_params['hdfs-dir'], strict=False) is not None:
        if not client.delete(config_params['hdfs-dir'], recursive=True):
            print('--- Failed to delete ' + config_params['hdfs-dir'])


def hadoop_cleanup_olk_artifacts(config_params):
    if olk_cluster_exists(config_params):
        print('--- ' + config_params['cluster-name'] + ' exists.')
        if config_params['RESTART']:
            print('--- Stopping ' + config_params['cluster-name'] + ' ...')
            stop_olk(config_params)
        else:
            exit(1)
    cleanup_hdfs_olk_cluster(config_params)


def hadoop_upload_olk_artifacts(config_params):
    print('--- Uploading artifacts to Hadoop DFS...')

    client = get_hdfs_client(config_params)

    # Upload files to hdfs
    client.makedirs(config_params['hdfs-dir'] + '/bin')

    openlookeng_package = config_params['hdfs-dir'] + '/openlookeng.tar.gz'
    client.upload(openlookeng_package, config_params['olk-server-package'], overwrite=True)
    hdfs_static_files = ['bin/launcher.py',
                         'bin/olk_container_entrypoint.py',
                         'conf/nginx.conf',
                         'bin/nginx_container_entrypoint.py',
                         'conf/olkonyarn_hdfs.properties']
    for file in hdfs_static_files:
        client.upload(config_params['hdfs-dir'] + '/' + file, config_params['STAGEDIR'] + '/' + file)

    client.upload(config_params['hdfs-dir'] + '/conf/coordinator_etc', config_params['STAGEDIR'] + '/conf/coordinator_etc')
    client.upload(config_params['hdfs-dir'] + '/conf/worker_etc', config_params['STAGEDIR'] + '/conf/worker_etc')
    client.upload(config_params['hdfs-dir'] + '/conf/nginx_etc', config_params['STAGEDIR'] + '/conf/nginx_etc')


def launch_olk_cluster(config_params):
    print("--- Launching openLooKeng Cluster...")
    launch_url = config_params['resource-manager-address'] + '/app/v1/services' + '?&user.name=' + \
                 config_params['yarn-user']

    headers = {'Content-type': 'application/json'}
    yarnfile = config_params['STAGEDIR'] + '/conf/openlookeng.json'
    with open(yarnfile) as f:
        json_args = json.load(f)
    response = None
    if config_params['request-authentication'].lower() == 'insecure' or config_params['request-authentication'].lower() == 'ssl':
        response = requests.post(launch_url, json=json_args, headers=headers)
    elif config_params['request-authentication'].lower() == 'kerberos':
        from requests_kerberos import HTTPKerberosAuth
        auth = HTTPKerberosAuth()
        response = requests.post(launch_url, json=json_args, headers=headers, auth=auth)

    try:
        response.raise_for_status()
        # access json content
        json_response = response.json()
        print(json_response)
    except Exception as e:
        print('--- Failed to launch openLooKeng cluster: ' + str(e))
    finally:
        if response is not None:
            response.close()


def monitor_olk_cluster(config_params):
    launch_olk_cluster(config_params)

    print("--- Monitoring openLooKeng coordinator to be ready...")
    retry_index = 0
    for retry_index in range(0, MAX_RETRY_COUNT):
        app_status_json = get_olk_cluster(config_params)
        if is_coordinator_stable(app_status_json):
            break
        else:
            print('--- ' + str(retry_index) + ' : Waiting for openLooKeng coordinator to be ready...')
        time.sleep(RETRY_INTERVAL)

    if retry_index == MAX_RETRY_COUNT - 1:
        print('--- Max retry count reached! OLK coordinator not ready!\n')
        exit(1)
    
    if config_params['use-nginx'] == 'true':
        # Use nginx url to reverse proxy to coordinators, look for file in hdfs
        client = get_hdfs_client(config_params)
        for retry_index in range(0, MAX_RETRY_COUNT):
            try:
                client.status(config_params['hdfs-dir'] + '/nginx-endpoint-url.txt')
                break
            except Exception as e:
                print('--- {0} : Waiting for nginx server to be ready: {1}'.format(retry_index, str(e)))
                time.sleep(RETRY_INTERVAL)
        if retry_index == MAX_RETRY_COUNT - 1:
            sys.stderr.write('Max retry count reached, nginx server not ready!\n')
            exit(1)

def print_olk_endpoint(config_params):
    if not olk_cluster_exists(config_params):
        return
    
    discovery_uri = None
    retry_index = 0

    # if nginx is deployed, just give nginx url.
    # if nginx is not deployed and ha is enabled, list all coordinators.
    # if nginx is not deployed and ha is not enabled, just give the latest coordinator in the list

    if is_nginx_deployed(config_params):
        # Use nginx url to reverse proxy to coordinators, look for file in hdfs
        if is_nginx_endpoint_reported(config_params):
            client = get_hdfs_client(config_params)
            with client.read(config_params['hdfs-dir'] + '/nginx-endpoint-url.txt') as reader:
                discovery_uri = str(reader.read(), 'utf-8')
        else:
            discovery_uri = 'No Nginx endpoint reported (error)'
        print('--- openLooKeng: ' + discovery_uri)

    elif is_ha_enabled(config_params):
        list_all_coordinators(config_params)
    
    else:
        for retry_index in range(0, MAX_RETRY_COUNT):
            try:
                client = get_hdfs_client(config_params)
                with client.read(config_params['HDFSSEEDSTOREPATH'] + '/seeds-resources.json') as reader:
                    seeds_json = reader.read()
                break
            except Exception as e:
                print(str(e))
                time.sleep(RETRY_INTERVAL)

        if retry_index == MAX_RETRY_COUNT - 1:
            print('Max retry count reached! OLK endpoint unknown!\n')
            exit(1)

        discovery_uri = get_discovery_uri(seeds_json)
        print('--- openLooKeng: ' + discovery_uri)


def print_summary(config_params):
    if not olk_cluster_exists(config_params):
        return
    display_cluster_status(get_olk_cluster(config_params))


def list_all_coordinators(config_params):
    '''
    Gives a list of all coordinators in the cluster. The coordinators should be reported 
    in seeds-resources.json, so this function will look in this file for the list of
    coordinators.
    '''
    client = get_hdfs_client(config_params)
    for retry_index in range(0, MAX_RETRY_COUNT):
        try:
            all_locations = ""
            with client.read(config_params['HDFSSEEDSTOREPATH'] + '/seeds-resources.json') as reader:
                seeds_str = reader.read()
                seeds_json = json.loads(seeds_str)
                for idx in range(len(seeds_json)):
                    seed = get_json_data(seeds_json, idx)
                    location = get_json_data(seed, 'location')
                    all_locations += "\n  {0}: {1}".format(idx, location)
                    
            print('--- All openLooKeng endpoints:{0}'.format(all_locations))
            break
        except Exception as e:
            print(str(e))
            time.sleep(RETRY_INTERVAL)

    if retry_index == MAX_RETRY_COUNT - 1:
        print('--- Max retry count reached, error reading seedstore file!')
        exit(1)


def start_olk(config_params):
    # Download olk server package
    if config_params['olk-server-package'] == 'download':
        download_olk_package(config_params)
    stage_files(config_params)
    hadoop_cleanup_olk_artifacts(config_params)
    hadoop_upload_olk_artifacts(config_params)
    monitor_olk_cluster(config_params)
    print_olk_endpoint(config_params)
    print_summary(config_params)


def get_olk_status(config_params):
    if not olk_cluster_exists(config_params):
        print('--- ' + config_params['cluster-name'] + ' does not exist.')
        return

    print_olk_endpoint(config_params)
    print_summary(config_params)


def restart_nginx_container(config_params):
    '''
    Flex the number of nginx containers to 0 and then to 1.
    '''
    request_json = {'nginx': {'name': 'nginx', 'number_of_containers': 0}}
    print('--- Flexing nginx component count to 0 and then back to 1...')
    print(json.dumps(request_json, indent=4, sort_keys=True))
    print('--- Press key in 20 seconds to abort...')
    i, o, e = select.select([sys.stdin], [], [], 20)
    if i:
        return
    
    scale_component(config_params, request_json['nginx'], 'nginx')

    request_json = {'nginx': {'name': 'nginx', 'number_of_containers': 1}}
    print('--- Flexing nginx component count back to 1...')
    print(json.dumps(request_json, indent=4, sort_keys=True))
    time.sleep(3)

    scale_component(config_params, request_json['nginx'], 'nginx')
    print_summary(config_params)

def flex_olk(config_params):
    if not olk_cluster_exists(config_params):
        print('--- ' + config_params['cluster-name'] + ' does not exist.')
        return

    request_json = {'coordinator': {'name': 'coordinator', 'number_of_containers': config_params['coordinators']},
                    'worker': {'name': 'worker', 'number_of_containers': config_params['workers']}}

    print('--- Received request to flex. ')
    print(json.dumps(request_json, indent=4, sort_keys=True))
    print('--- Press key in 20 seconds to abort...')
    i, o, e = select.select([sys.stdin], [], [], 20)
    if i:
        return

    for component in ['worker', 'coordinator']:
        scale_component(config_params, request_json[component], component)

    get_olk_status(config_params)

def scale_component(config_params, request_json, component):
    '''
    Submits a flex request for the given component, using the given json dict.
    Example: request_json={ 'name': 'coordinator', 'number_of_containers': 1 }, component='coordinator'
    '''
    flex_url = config_params['resource-manager-address'] + '/app/v1/services/' + \
                   config_params['cluster-name'] + '/components/' + component + '?&user.name=' + \
                   config_params['yarn-user']

    headers = {'Content-type': 'application/json'}
    response = None
    if config_params['request-authentication'].lower() == 'insecure' or config_params['request-authentication'].lower() == 'ssl':
        response = requests.put(flex_url,
                                json=request_json,
                                headers=headers)
    elif config_params['request-authentication'].lower() == 'kerberos':
        from requests_kerberos import HTTPKerberosAuth
        auth = HTTPKerberosAuth()
        response = requests.put(flex_url,
                                json=request_json,
                                headers=headers, auth=auth)

    try:
        response.raise_for_status()
            # access json content
        json_response = response.json()
        print(json_response)
    except Exception as e:
        print('--- Failed to scale OLK cluster component: {0}'.format(component))
        print(str(e))
    finally:
        if response is not None:
            response.close()


def stop_olk(config_params):
    if not olk_cluster_exists(config_params):
        print('--- ' + config_params['cluster-name'] + ' does not exist.')
        return

    stop_url = config_params['resource-manager-address'] + '/app/v1/services/' + config_params['cluster-name'] + \
               '?&user.name=' + config_params['yarn-user']
    response = None
    if config_params['request-authentication'].lower() == 'insecure' or config_params['request-authentication'].lower() == 'ssl':
        response = requests.delete(stop_url)
    elif config_params['request-authentication'].lower() == 'kerberos':
        from requests_kerberos import HTTPKerberosAuth
        auth = HTTPKerberosAuth()
        response = requests.delete(stop_url, auth=auth)

    try:
        response.raise_for_status()
        # access json content
        json_response = response.json()
        print(json_response)
    except Exception as e:
        print('--- Failed to stop OLK cluster')
        print(str(e))
        exit(1)
    finally:
        if response is not None:
            response.close()


def versiontuple(v):
    return tuple(map(int, (v.split("."))))


def read_latest_download_version():
    print('--- Downloading latest openLooKeng version info')
    version = None
    response = None
    try:
        response = requests.get('https://download.openlookeng.io')
        response.raise_for_status()
        version_str = "0"
        version_tuple = versiontuple(version_str)
        for line in response.text.splitlines():
            new_version_str = re.findall('010|\d+\.\d+\.\d+', line)
            if len(new_version_str) <= 0:
                continue
            new_version_str = new_version_str[0]
            new_version_tuple = versiontuple(str(new_version_str))
            if new_version_tuple > version_tuple:
                version_tuple = new_version_tuple
                version_str = new_version_str
    except Exception as e:
        print('--- Failed to obtain latest version')
        print(str(e))
    finally:
        response.close()
    return version_str


def download_olk_package(config_params):
    version = read_latest_download_version()
    if version is None:
        exit(1)
    response = None
    try:
        os.mkdir('/tmp/openlookeng')
        config_params['olk-server-package'] = "/tmp/openlookeng/openlookeng.tar.gz"
        if not os.path.exists(config_params['olk-server-package']):
            print("--- Downloading OLK server package...")
            url = 'https://download.openlookeng.io/' + str(version) + '/hetu-server-' + str(version) + '.tar.gz'

            with open(config_params['olk-server-package'], "wb") as f:
                response = requests.get(url)
                total_length = response.headers.get('content-length')

                if total_length is None:  # no content length header
                    f.write(response.content)
                else:
                    dl = 0
                    total_length = int(total_length)
                    for data in response.iter_content(chunk_size=4096):
                        dl += len(data)
                        f.write(data)
                        done = int(50 * dl / total_length)
                        print("\r[%s%s]" % ('=' * done, ' ' * (50 - done)))
                        sys.stdout.flush()

        print("--- Validate hetu-server package using sha256...")

        url = 'https://download.openlookeng.io/' + str(version) + '/hetu-server-' + str(version) + '.tar.gz.sha256sum'
        response = requests.get(url)
        received_sha256sum = response.content

        sha256_hash = hashlib.sha256()
        with open(config_params['olk-server-package'], "rb") as f:
            # Read and update hash string value in blocks of 4K
            for byte_block in iter(lambda: f.read(4096), b""):
                sha256_hash.update(byte_block)

        if received_sha256sum != sha256_hash:
            sys.stderr.write('ERROR: Delete possibly corrupted file: ' + config_params['olk-server-package'])
            exit(1)
    except Exception as e:
        print('--- Failed to download openLooKeng server package')
        print(str(e))
    finally:
        if response is not None:
            response.close()


def command_line_parser():
    # Static configuration parameters
    work_dir = os.path.dirname(os.path.realpath(__file__))
    work_dir = os.path.dirname(work_dir)
    config_params = {
        'WORKDIR': work_dir,
        'STAGEDIR': work_dir + '/stage',
        'config': None,
        'yarn-user': None,
        'hdfs-user': None,
        'resource-manager-address': None,
        'name-node-address': None,
        'olk-server-package': None,
        'olk-server-port': None,
        'olk-worker-port': None,
        'cluster-name': None,
        'cluster-prefix': None,
        'hdfs-dir': None,
        'coordinators': None,
        'workers': None,
        'olk-yarnfile': None,
        'request-authentication': None,
        'kerberos-principal': None,
        'kerberos-keytab': None,
        'high-availability': None,
        'use-nginx': None,
        'nginx-only': None,
        'nginx-port': None,
        'fail-if-nginx-port-in-use': None,
        'nginx-reload-check-interval': None
    }

    parser = argparse.ArgumentParser(description='Administrator tools for deploying openLooKeng on YARN')
    
    subparsers = parser.add_subparsers(title="commands", dest="command")
    start_parser = subparsers.add_parser(name="start", description="Start an OLK cluster.")
    restart_parser = subparsers.add_parser(name="restart", description="Restart an existing OLK cluster.")
    status_parser = subparsers.add_parser(name="status", description="See the status of an existing OLK cluster.")
    flex_parser = subparsers.add_parser(name="flex", description="Scale an existing OLK cluster up or down.")
    stop_parser = subparsers.add_parser(name="stop", description="Stop an existing OLK cluster.")

    for subpar in (start_parser, restart_parser, status_parser, flex_parser, stop_parser):
        subpar.add_argument('--config', '-c', dest='config',
                            metavar="/filepath/file.json",
                            help='Path to the JSON olk_on_yarn configuration file',
                            default=config_params['WORKDIR'] + "/conf/olk_on_yarn_default.json")
    
    for subpar in (start_parser, restart_parser, status_parser, flex_parser, stop_parser):
        subpar.add_argument('--yarn-user', dest='yarn-user', metavar='username',
                            help='YARN username', default=None)

    for subpar in (start_parser, restart_parser, flex_parser, stop_parser):
        subpar.add_argument('--hdfs-user', dest='hdfs-user', metavar='username',
                            help='Hadoop username', default=None)    
    
    for subpar in (start_parser, restart_parser, status_parser, flex_parser, stop_parser):
        subpar.add_argument('--resource-manager-address', '-r', metavar='ip:port',
                            dest='resource-manager-address',
                            help='URI for YARN\'s resource manager', default=None)

    for subpar in (start_parser, restart_parser, status_parser, flex_parser, stop_parser):
        subpar.add_argument('--name-node-address', metavar='ip:port',
                            dest='name-node-address',
                            help='URI for Hadoop\'s name node', default=None)

    for subpar in (start_parser, restart_parser):
        subpar.add_argument('--olk-server-package', '-sp',
                            metavar='openlookeng.tar.gz',
                            dest='olk-server-package',
                            help='Server package for openLooKeng', default=None)

    for subpar in (start_parser, restart_parser):
        subpar.add_argument('--olk-server-port', '-s', metavar='port', type=int,
                            dest='olk-server-port',
                            help='Initial OLK Service port for external access',
                            default=None)

    for subpar in (start_parser, restart_parser):
        subpar.add_argument('--olk-worker-port', metavar='port', type=int,
                            dest='olk-worker-port',
                            help='Initial OLK service port for workers communication',
                            default=None)

    start_parser.add_argument('--cluster-prefix', '-p', metavar='prefix',
                        dest='cluster-prefix',
                        help='OLK cluster prefix. A random ID will be appended to this.',
                        default=None)

    for subpar in (restart_parser, status_parser, flex_parser, stop_parser):
        subpar.add_argument('--cluster-name', '-n', metavar='name',
                            dest='cluster-name',
                            help='OLK cluster name.',
                            default=None)
    
    for subpar in (start_parser, restart_parser):
        subpar.add_argument('--hdfs-dir', metavar='dirname',
                            dest='hdfs-dir',
                            help='Root HDFS directory for storing OLK artifacts',
                            default=None)
    
    for subpar in (start_parser, restart_parser, flex_parser):
        subpar.add_argument('--workers', '-w', metavar='N',
                            type=int, dest='workers',
                            help='Deploy count for OLK workers', default=None)

    for subpar in (start_parser, restart_parser):
        subpar.add_argument('--olk-yarnfile', metavar='openlookeng.json.template',
                           dest='olk-yarnfile',
                           help='JSON yarnfile for deploying OLK', default=None)

    for subpar in (start_parser, restart_parser, status_parser, flex_parser, stop_parser):
        subpar.add_argument('--request-authentication', metavar='[insecure|kerberos|ssl]',
                            dest='request-authentication',
                            help='Request authentication framework', default=None,
                            choices=['insecure', 'kerberos', 'ssl'])

    for subpar in (start_parser, restart_parser, status_parser, flex_parser, stop_parser):
        subpar.add_argument('--kerberos-principal', metavar='principal_name',
                            dest='kerberos-principal', help='Principal name used for Kerberos authentication', default=None)

    for subpar in (start_parser, restart_parser, status_parser, flex_parser, stop_parser):
        subpar.add_argument('--kerberos-keytab', metavar='filepath',
                            dest='kerberos-keytab',
                            help='Keytab file for Kerberos authentication', default=None)
    
    for subpar in (start_parser, restart_parser, flex_parser):
        subpar.add_argument('--coordinators', '-cn', metavar='N',
                            type=int, dest='coordinators',
                            help='Deploy count for OLK coordinators', default=None)
    
    for subpar in (start_parser, restart_parser):
        subpar.add_argument('--high-availability', '-ha', metavar='true|false',
                            dest='high-availability',
                            help='Flag for enabling HA for OLK deployment', default=None)
    
    for subpar in (start_parser, restart_parser):
        subpar.add_argument('--use-nginx', metavar='true|false', 
                            dest='use-nginx', 
                            help='Whether or not to deploy nginx as a reverse proxy and load balancer. ' + 
                                 'Defaults to true if HA enabled, otherwise defaults to false. ' + 
                                 'Requires nginx to be installed in the cluster.')
    
    restart_parser.add_argument('--nginx-only', metavar='true|false',
                                dest='nginx-only',
                                help='If true, restarts only the nginx container.', default=None)

    for subpar in (start_parser, restart_parser):
        subpar.add_argument('--nginx-port', metavar='port1,port2,...,portN',
                            dest='nginx-port',
                            help='Comma-separated series of static ports to try for the nginx endpoint, if deploying nginx.', default=None)
        subpar.add_argument('--fail-if-nginx-port-in-use', metavar='true|false',
                            dest='fail-if-nginx-port-in-use',
                            help='Whether or not to fail the nginx component if the provided static nginx ports are all taken. ' + 
                                 'If false, the script will try to find a free port if the given ones are taken. ' +
                                 'The default setting is false.', 
                            default=None)
        subpar.add_argument('--nginx-reload-check-interval', metavar='N',
                            dest='nginx-reload-check-interval',
                            help='Interval for checking whether nginx should be reloaded, if deployed. Every N seconds, ' +
                                 'the script checks the list of coordinators to see whether it has changed, and if so, updates nginx to ' +
                                 'use the new list of coordinators. The default is 5 seconds.',
                            default=None,
                            type=int)

    cli_args = parser.parse_args()
    cli_args_dict = vars(cli_args)
    config_params.update(cli_args_dict)

    if config_params["command"] is None:
        print("--- A command is required. {start,restart,flex,status,stop}")
        exit(1)

    if config_params["command"] in ['restart', 'status', 'flex', 'stop'] and config_params["cluster-name"] is None:
        print("--- Cluster name needs to be given with the '{0}' command.".format(config_params["command"]))
        exit(1)

    if not os.path.exists(config_params['config']):
        if not os.path.exists(config_params['WORKDIR'] + config_params['config']):
            if not os.path.exists(config_params['WORKDIR'] + '/conf/' + config_params['config']):
                sys.stderr.write("Failed to locate " + str(config_params['config']))
                exit(1)
            config_params['config'] = config_params['WORKDIR'] + '/conf/' + config_params['config']
        config_params['config'] = config_params['WORKDIR'] + config_params['config']

    with open(cli_args_dict['config']) as f:
        json_args = json.load(f)

    for key in config_params:
        if config_params[key] is None and key in json_args.keys():
            config_params[key] = json_args[key]

    if config_params['yarn-user'] is None:
        config_params['yarn-user'] = getpass.getuser()
    if config_params['hdfs-user'] is None:
        config_params['hdfs-user'] = getpass.getuser()

    # Concatenate prefix and uuid if command is start
    if config_params["command"] == "start" and (config_params["cluster-prefix"] is None or config_params["cluster-prefix"] == ""):
        print("--- Nonempty cluster prefix needs to be given with the 'start' command.")
        exit(1)
    elif config_params["command"] == "start":
        config_params["cluster-name"] = config_params["cluster-prefix"] + "-" + str(uuid.uuid4())
    print("--- cluster name: {0}".format(config_params["cluster-name"]))

    # Update hdfs-dir, seedstore path
    config_params['hdfs-dir'] = config_params['hdfs-dir'] + '/' + config_params['hdfs-user'] + '/' + \
                                config_params['cluster-name']
    config_params['HDFSSEEDSTOREPATH'] = '/opt/hetu/seedstore/' + \
                                         config_params['cluster-name']

    # Default settings
    if config_params['coordinators'] is None:
        config_params['coordinators'] = 1
    if config_params['high-availability'] is None:
        config_params['high-availability'] = 'false'
    if config_params['nginx-only'] is None:
        config_params['nginx-only'] = 'false'
    if config_params['fail-if-nginx-port-in-use'] is None:
        config_params['fail-if-nginx-port-in-use'] = 'false'
    if config_params['nginx-reload-check-interval'] is None:
        config_params['nginx-reload-check-interval'] = 5
    
    # Default setting for use-nginx
    if config_params['use-nginx'] is None and config_params['high-availability'] == 'true':
        config_params['use-nginx'] = 'true'
    elif config_params['use-nginx'] is None:
        config_params['use-nginx'] = 'false'

    # If nginx-port given, check up-front that all of them are actually integers
    if config_params['nginx-port'] is not None:
        for port in config_params['nginx-port'].split(','):
            port = port.strip()
            try:
                int(port)
            except:
                print('--- nginx port {0} cannot be converted to an integer port.'.format(port))
                exit(1)
    
    # Might need to convert to integer values if these are given through olk_on_yarn_default.json instead of command line
    config_params['coordinators'] = int(config_params['coordinators'])
    config_params['workers'] = int(config_params['workers'])

    # Enforce that coordinators deployed can't be 0
    if config_params['command'] in ['start', 'restart', 'flex'] and config_params['coordinators'] == 0:
        print('--- Cannot deploy 0 coordinators.')
        exit(1)
    
    # Enforce high availability for cn > 1
    if config_params['command'] in ['start', 'restart'] and config_params["coordinators"] > 1 and not config_params["high-availability"] == "true":
        print("--- High availability needs to be set to \"true\" in order to deploy multiple coordinators.")
        exit(1)
    if config_params['command'] == 'flex' and config_params['coordinators'] > 1 and not is_ha_enabled(config_params):
        print('--- Cluster needs to be deployed with high availability to have multiple coordinators. '
             'The cluster is either not HA or not present.')
        exit(1)

    return config_params


if __name__ == '__main__':
    config_params = command_line_parser()

    if config_params['command'].lower() == "start":
        config_params['RESTART'] = False
        start_olk(config_params)
    if config_params['command'].lower() == "restart":
        config_params['RESTART'] = True
        if config_params['nginx-only'] == 'true':
            restart_nginx_container(config_params)
            pass
        else:
            start_olk(config_params)
    elif config_params['command'].lower() == "status":
        get_olk_status(config_params)
    elif config_params['command'].lower() == "flex":
        flex_olk(config_params)
    elif config_params['command'].lower() == "stop":
        stop_olk(config_params)
