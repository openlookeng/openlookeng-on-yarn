##
# Copyright (C) 2018-2021. Huawei Technologies Co., Ltd. All rights reserved.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##

# Python library for container entrypoint script

import json
import os.path
import shutil
import socket
import subprocess
import sys
import time

import hdfs
import requests

# ## Environment Variables
# # NGINX_CONF_HOME : $PWD,
# # NGINX_LAUNCH_CMD : nginx

_JSON_DEFAULT_OUTPUT = '-'
MAX_RETRY_COUNT_HDFS = 100
MAX_RETRY_COUNT_SEEDS_JSON = 10
RETRY_INTERVAL = 20
NGINX_CONF_HOME = os.getcwd()
NGINX_LAUNCH_CMD = os.getenv('NGINX_LAUNCH_CMD')
OLK_RUN_HOME = os.getenv('OLK_RUN_HOME')


def get_json_data(json_obj, *keys):
    data = json_obj
    try:
        for key in keys:
            data = data[key]
    except:
        return _JSON_DEFAULT_OUTPUT

    if data is None:
        return _JSON_DEFAULT_OUTPUT

    return data


def file_match_and_replace_string(filename, match_pattern, replace_pattern):
    with open(filename, "rt") as input_file:
        data = input_file.read()

    data = data.replace(str(match_pattern), str(replace_pattern))

    with open(filename, "wt") as output_file:
        output_file.write(data)


def create_olk_etc_dir():
    '''
    Creates the olk etc directory, which will be used to read hdfs properties
    '''
    if os.path.exists(OLK_RUN_HOME):
        shutil.rmtree(OLK_RUN_HOME)

    os.mkdir(OLK_RUN_HOME)
    # /bin directory created during copy
    os.mkdir(OLK_RUN_HOME + '/etc')


def copy_olk_etc_files():
    '''
    Copies files for hdfs properties
    '''
    # Create copy of OLK configuration files
    for root, dirnames, filenames in os.walk(os.getcwd() + "/conf"):
        for filename in filenames:
            if filename.startswith('olkonyarn_'):
                shutil.copy2(os.path.join(root, filename), OLK_RUN_HOME + '/' + filename)
            else:
                shutil.copy2(os.path.join(root, filename), OLK_RUN_HOME + '/etc/' + filename)


def load_properties(filepath, sep='=', comment_char='#'):
    """
    Read the file passed as parameter as a properties file.
    """
    props = {}
    with open(filepath, "rt") as f:
        for line in f:
            l = line.strip()
            if l and not l.startswith(comment_char):
                key_value = l.split(sep)
                key = key_value[0].strip()
                value = sep.join(key_value[1:]).strip().strip('"')
                props[key] = value
    return props


def get_cluster_name():
    seed_store_file = OLK_RUN_HOME + '/etc/seed-store.properties'
    seed_store_properties = load_properties(seed_store_file)
    name = seed_store_properties['seed-store.cluster']
    return name

def get_olk_cluster():
    deployment_params = read_olkonyarn_deployment_properties()
    url = deployment_params['resource-manager-address'] + '/app/v1/services/' + get_cluster_name() \
          + '?&user.name=' + deployment_params['yarn-user']
    response = None
    data = None
    if deployment_params['request-authentication'].lower() == 'insecure' or deployment_params['request-authentication'].lower() == 'ssl':
        response = requests.get(url)
    elif deployment_params['request-authentication'].lower() == 'kerberos':
        from requests_kerberos import HTTPKerberosAuth
        auth = HTTPKerberosAuth()
        response = requests.get(url, auth=auth)

    if response is not None:
        data = response.json()
        response.close()

    return data


def olk_cluster_exists():
    olk_cluster_info = get_olk_cluster()
    cluster_name = get_json_data(olk_cluster_info, "name")
    if cluster_name == get_cluster_name():
        return True

    return False


def get_number_of_coordinators():
    '''
    Returns the number of coordinators in the olk service.
    '''
    json_data = get_olk_cluster()
    components = get_json_data(json_data, "components")
    if components == _JSON_DEFAULT_OUTPUT:
        return 0

    num_coordinators = 0
    for i in range(len(components)):
        component = get_json_data(components, i)
        if component == " - ":
            continue
        component_name = get_json_data(component, 'name')
        if component_name != 'coordinator':
            continue
        num_coordinators = get_json_data(component, 'number_of_containers')
        print('Number of coordinators: {0}'.format(num_coordinators))
        
    return int(num_coordinators)


def read_olkonyarn_deployment_properties():
    filename = OLK_RUN_HOME + '/olkonyarn_hdfs.properties'
    olkonyarn_hdfs_properties = load_properties(filename)
    return olkonyarn_hdfs_properties


def get_hdfs_client():
    deployment_params = read_olkonyarn_deployment_properties()

    if deployment_params['request-authentication'].lower() == 'insecure' or deployment_params['request-authentication'].lower() == 'ssl':
        return hdfs.InsecureClient(deployment_params['name-node-address'],
                                   user=deployment_params['hdfs-user'])
    elif deployment_params['request-authentication'].lower() == 'kerberos':
        import requests
        session = requests.Session()
        from hdfs.ext.kerberos import KerberosClient
        return KerberosClient(url=deployment_params['name-node-address'], session=session)

    print('--- Failed to retrieve hdfs client')
    exit(1)

def port_is_in_use(ip, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.bind((ip, int(port)))  # p1
        s.close()  # p1 is now free for other apps
        return False
    except:
        return True


def replace_template_strings_in_nginx_conf(nginx_port, nginx_ip, servers_str):
    '''
    Replaces all template strings in nginx.conf.
    '''
    os.chmod(NGINX_CONF_HOME + '/nginx.conf', 0o700)
    file_match_and_replace_string(NGINX_CONF_HOME + "/nginx.conf", "_NGINX_PROPERTIES_TEMPLATE_NGINX_CONF_HOME", NGINX_CONF_HOME)
    file_match_and_replace_string(NGINX_CONF_HOME + "/nginx.conf", "_NGINX_PROPERTIES_TEMPLATE_UPSTREAM_SERVERS", servers_str)
    file_match_and_replace_string(NGINX_CONF_HOME + "/nginx.conf", "_NGINX_PROPERTIES_TEMPLATE_PORT", nginx_port)
    file_match_and_replace_string(NGINX_CONF_HOME + "/nginx.conf", "_NGINX_PROPERTIES_TEMPLATE_IP", nginx_ip)


def get_seeds_resources_modified_time(hdfs_client):
    '''
    Returns the modified time for the seeds-resources.json file in hdfs
    '''
    seeds_filestatus = hdfs_client.status('/opt/hetu/seedstore/' + get_cluster_name() + '/seeds-resources.json')
    return seeds_filestatus['modificationTime']

def get_servers_str(hdfs_client):
    '''
    Reads the seeds from seeds-resources.json and returns a string of servers to put in nginx.conf
    '''
    cn_seeds_json = None
    with hdfs_client.read('/opt/hetu/seedstore/' + get_cluster_name() + '/seeds-resources.json') as reader:
        seeds_str = reader.read()
        cn_seeds_json = json.loads(seeds_str)
    servers_str = ""
    for cn_seed in cn_seeds_json:
        print("Add cn {0} to servers list in nginx.conf".format(cn_seed))
        servers_str += "server {0};\n		".format(cn_seed.get("location").split("://")[1]) # http(s)://ip:port, the http should not be there
    return servers_str


def update_nginx_conf_and_launch():
    hdfs_client = get_hdfs_client()
    deployment_params = read_olkonyarn_deployment_properties()

    # Wait until the seeds file exists

    retry_index = 0
    for retry_index in range(0, MAX_RETRY_COUNT_HDFS):
        try:
            hdfs_client.status('/opt/hetu/seedstore/' + get_cluster_name() + '/seeds-resources.json')
            break
        except Exception as e:
            print(str(e))
            time.sleep(RETRY_INTERVAL)

    if retry_index == MAX_RETRY_COUNT_HDFS - 1:
        sys.stderr.write('Max retry count reached, cannot access seeds-resources.json!')
        exit(1)
    
    # Find appropriate nginx port
    nginx_port = None
    nginx_ip = socket.gethostbyname(socket.gethostname())
    if deployment_params['nginx-port'] is not None:
        for port in deployment_params['nginx-port'].split(','):
            try:
                int(port.strip())
            except:
                continue
            if not port_is_in_use(nginx_ip, int(port.strip())):
                nginx_port = int(port.strip())
                break

    if nginx_port is None and deployment_params['fail-if-nginx-port-in-use'] == 'true':
        sys.stderr.write('None of the given nginx ports were free: {0}\n'.format(deployment_params['nginx-port']))
        exit(1)
    elif nginx_port is None:
        # Find an open port starting from 1024
        nginx_port = 1024
        while port_is_in_use(nginx_ip, nginx_port):
            nginx_port += 1
    
    print("Using {0}:{1} as nginx url".format(nginx_ip, nginx_port))
    
    if not olk_cluster_exists():
        print('The cluster {0} does not exist.'.format(get_cluster_name()))
        exit(1)
    num_coordinators = get_number_of_coordinators()

    # Wait until all coordinators have written their ip addresses to seeds-resources.json
    retry_index = 0
    cn_seeds_json = None
    for retry_index in range(0, MAX_RETRY_COUNT_SEEDS_JSON):
        seeds_str = None
        with hdfs_client.read('/opt/hetu/seedstore/' + get_cluster_name() + '/seeds-resources.json') as reader:
            seeds_str = reader.read()
        cn_seeds_json = json.loads(seeds_str)
        if len(cn_seeds_json) >= num_coordinators:
            print("All {0} coordinators have written their IP addresses to seeds-resources.json".format(num_coordinators))
            break
        print("{0} out of {1} coordinators have written their IP addresses to seeds-resources.json".format(len(cn_seeds_json), num_coordinators))
        time.sleep(RETRY_INTERVAL)
    
    if retry_index == MAX_RETRY_COUNT_SEEDS_JSON - 1:
        print('Max retry count reached but not all coordinators have declared their IP addresses, continuing anyway')

    # (re)download the nginx.conf file
    try:
        hdfs_client.download(deployment_params['hdfs-dir'] + '/conf/nginx.conf', NGINX_CONF_HOME + '/nginx.conf', overwrite=True)
    except Exception as e:
        sys.stderr.write('Failed to download nginx.conf from hdfs: {0}\n'.format(str(e)))
        exit(1)

    # Replace template strings in nginx.conf
    servers_str = get_servers_str(hdfs_client)
    replace_template_strings_in_nginx_conf(nginx_port, nginx_ip, servers_str)
    
    # Launch nginx
    process = subprocess.Popen([NGINX_LAUNCH_CMD, "-c", NGINX_CONF_HOME + "/nginx.conf"])
    communicate_nginx_endpoint_hdfs(nginx_ip, nginx_port)

    wait_and_check_or_update_nginx(deployment_params, nginx_port, nginx_ip, process)

    returncode = process.poll()
    if returncode != 0:
        sys.stderr.write('Nginx process failed, return code is {0}\n'.format(returncode))
        delete_nginx_endpoint_hdfs()
        exit(1)
    else:
        print('Nginx process finished with exit code 0')
        delete_nginx_endpoint_hdfs()
        exit(1) # not intended to just end, unless service is being stopped by user


def communicate_nginx_endpoint_hdfs(ip, port):
    '''
    Communicate the nginx endpoint over hdfs. The intent is for this to be read by olk_on_yarn.py so the user can see
    '''
    hdfs_client = get_hdfs_client()
    deployment_params = read_olkonyarn_deployment_properties()
    endpoint_file = NGINX_CONF_HOME + '/nginx-endpoint-url.txt'
    with open(endpoint_file, 'w') as f:
        f.write('http://{0}:{1}'.format(ip, port))
    try:
        hdfs_client.upload(deployment_params['hdfs-dir'] + '/nginx-endpoint-url.txt', endpoint_file, overwrite=True)
    except Exception as e:
        sys.stderr.write('Failed to upload nginx-endpoint-url.txt: {0}'.format(str(e)))
        exit(1)
    print('Reported nginx endpoint url http://{0}:{1}'.format(ip, port))


def delete_nginx_endpoint_hdfs():
    '''
    Deletes the nginx endpoint file (originally uploaded to HDFS by communicate_nginx_endpoint_hdfs) in HDFS.
    '''
    hdfs_client = get_hdfs_client()
    deployment_params = read_olkonyarn_deployment_properties()
    hdfs_client.delete(deployment_params['hdfs-dir'] + '/nginx-endpoint-url.txt')


def wait_and_check_or_update_nginx(deployment_params, nginx_port, nginx_ip, process):
    '''
    Check whether nginx is still running every once in a while, exit if nginx is no longer running.
    Also checks whether the list of coordinators changed; if it did, nginx needs to be reloaded.

    deployment_params: Dictionary of deployment params from read_olkonyarn_deployment_properties()
    nginx_port: nginx port to use in nginx.conf
    nginx_ip: nginx ip to use in nginx.conf
    process: Popen process object
    '''
    hdfs_client = get_hdfs_client()
    wait_time = int(deployment_params['nginx-reload-check-interval'])
    original_modified_time = get_seeds_resources_modified_time(hdfs_client)

    while process.poll() is None:
        hdfs_client = get_hdfs_client()
        modified_time = get_seeds_resources_modified_time(hdfs_client)
        if modified_time > original_modified_time:

            # Check that seeds-resources.json is stable and unlikely to change further
            if not is_seeds_resources_json_stable(hdfs_client, wait_time):
                continue

            print('Reloading nginx')
            try:
                hdfs_client.download(deployment_params['hdfs-dir'] + '/conf/nginx.conf', NGINX_CONF_HOME + '/nginx.conf', overwrite=True)
                servers_str = get_servers_str(hdfs_client)
                replace_template_strings_in_nginx_conf(nginx_port, nginx_ip, servers_str)
                returncode = subprocess.call([NGINX_LAUNCH_CMD, '-c', NGINX_CONF_HOME + '/nginx.conf', '-s', 'reload'])
                if returncode == 0:
                    original_modified_time = modified_time
                print('Reload nginx return code (if nonzero, will retry in {1} seconds): {0}'.format(returncode, wait_time))
            except Exception as e:
                print('Failed to reload nginx, will try again in {1} seconds: {0}'.format(str(e), wait_time))
                
        time.sleep(wait_time)


def is_seeds_resources_json_stable(hdfs_client, wait_time):
    '''
    Check modified time of seeds-resources.json, wait wait_time seconds, then check the modified time again. 
    Returns true if both times are the same, otherwise returns false.
    '''
    modified_time = get_seeds_resources_modified_time(hdfs_client)
    time.sleep(wait_time)
    new_modified_time = get_seeds_resources_modified_time(hdfs_client)
    return new_modified_time == modified_time


if __name__ == '__main__':
    if NGINX_CONF_HOME is None:
        sys.stderr.write("Failed to locate nginx configuration directory!\n")
        exit(1)

    if NGINX_LAUNCH_CMD is None:
        sys.stderr.write("Failed to identify nginx launch command!\n")
        exit(1)

    if OLK_RUN_HOME is None:
        sys.stderr.write("Failed to locate OLK run directory!\n")
        exit(1)

    create_olk_etc_dir()
    copy_olk_etc_files()
    update_nginx_conf_and_launch()
