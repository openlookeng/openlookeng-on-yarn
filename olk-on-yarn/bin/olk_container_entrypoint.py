##
# Copyright (C) 2018-2021. Huawei Technologies Co., Ltd. All rights reserved.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##

# Python library for container entrypoint script

import hashlib
import json
import os.path
import shutil
import socket
import subprocess
import sys
import time

import hdfs

# ## Environment Variables
# # OLK_RUN_HOME : $PWD/openlookeng",
# # OLK_LIB_HOME : $PWD/lib/openlookeng"

MAX_RETRY_COUNT = 100
RETRY_INTERVAL = 20
OLK_RUN_HOME = os.getenv('OLK_RUN_HOME')
OLK_LIB_HOME = os.getenv('OLK_LIB_HOME')
COORDINATOR = os.getenv('COORDINATOR')


def get_discovery_uri(seeds_json):
    try:
        arg_json_obj = json.loads(seeds_json)
    except ValueError:
        return

    latest_location = None
    latest_timestamp = None
    for seed_index in range(len(arg_json_obj)):
        temp_seed = arg_json_obj[seed_index]
        if latest_timestamp is None or latest_timestamp < temp_seed["timestamp"]:
            latest_location = temp_seed["location"]
            latest_timestamp = temp_seed["timestamp"]
    return latest_location


def file_match_and_replace_string(filename, match_pattern, replace_pattern):
    with open(filename, "rt") as input_file:
        data = input_file.read()

    data = data.replace(str(match_pattern), str(replace_pattern))

    with open(filename, "wt") as output_file:
        output_file.write(data)


def create_olk_run_dir():
    if os.path.exists(OLK_RUN_HOME):
        shutil.rmtree(OLK_RUN_HOME)

    os.mkdir(OLK_RUN_HOME)
    # /bin directory created during copy
    # /etc will be created upon download
    os.mkdir(OLK_RUN_HOME + '/data')
    os.mkdir(OLK_RUN_HOME + '/log')

    hetu_server_dir = None
    for root, dirnames, filenames in os.walk(os.getcwd() + "/lib"):
        for dirname in dirnames:
            if 'hetu-server' in dirname:
                hetu_server_dir = os.path.join(root, dirname)

    if hetu_server_dir is None:
        sys.stderr.write("Failed to setup the run directory!")
        exit(1)

    try:
        if not os.path.exists(OLK_LIB_HOME):
            os.symlink(hetu_server_dir, OLK_LIB_HOME)
    except FileExistsError as e:
        print(str(e))

    os.symlink(OLK_LIB_HOME + '/lib', OLK_RUN_HOME + '/lib')
    os.symlink(OLK_LIB_HOME + '/plugin', OLK_RUN_HOME + '/plugin')


def copy_olk_instance_files():
    shutil.copytree(OLK_LIB_HOME + '/bin/', OLK_RUN_HOME + '/bin', symlinks=False)

    # Create copy of OLK configuration files
    for root, dirnames, filenames in os.walk(os.getcwd() + "/conf"):
        for filename in filenames:
            if filename.startswith('olkonyarn_'):
                shutil.copy2(os.path.join(root, filename), OLK_RUN_HOME + '/' + filename)
    
    # Download olk configuration files from hdfs

    client = get_hdfs_client()
    deployment_params = read_olkonyarn_deployment_properties()

    if COORDINATOR:
        client.download(deployment_params['hdfs-dir'] + '/conf/coordinator_etc', OLK_RUN_HOME + '/etc', overwrite=True)
    else:
        client.download(deployment_params['hdfs-dir'] + '/conf/worker_etc', OLK_RUN_HOME + '/etc', overwrite=True)

    # Update the launcher script to resolve jmx port conflict
    shutil.move(OLK_RUN_HOME + '/bin/launcher.py', OLK_RUN_HOME + '/bin/launcher.py.orig')
    shutil.copy2(os.getcwd() + '/launcher.py', OLK_RUN_HOME + '/bin/')
    os.chmod(OLK_RUN_HOME + '/bin/', 0o755)


def update_olk_run_files():
    # Update hdfs config resources
    if os.path.exists(OLK_RUN_HOME + '/etc/filesystem/hdfs-config-default.properties'):
        os.chmod(OLK_RUN_HOME + '/etc/filesystem/hdfs-config-default.properties', 0o700)
        file_match_and_replace_string(OLK_RUN_HOME + '/etc/filesystem/hdfs-config-default.properties',
                                      '_OLK_PROPERTIES_TEMPLATE_HDFS_CONFIG_RESOURCES',
                                      OLK_RUN_HOME + '/etc/filesystem/core-site.xml,' +
                                      OLK_RUN_HOME + '/etc/filesystem/hdfs-site.xml')

    if COORDINATOR:
        os.chmod(OLK_RUN_HOME + '/etc/config.properties', 0o700)
        file_match_and_replace_string(OLK_RUN_HOME + '/etc/config.properties',
                                      '_OLK_PROPERTIES_TEMPLATE_CN_DISCOVERY_IP',
                                      socket.gethostbyname(socket.gethostname()))

    if os.path.exists(OLK_RUN_HOME + "/etc/node.properties"):
        os.chmod(OLK_RUN_HOME + "/etc/node.properties", 0o700)
        file_match_and_replace_string(OLK_RUN_HOME + "/etc/node.properties", 
                                      "_OLK_PROPERTIES_TEMPLATE_NODE_ENVIRONMENT", 
                                      hashlib.md5(get_cluster_name().encode()).hexdigest())

    for root, dirnames, filenames in os.walk(OLK_RUN_HOME + "/etc"):
        for filename in filenames:
            abs_path = os.path.join(root, filename)
            os.chmod(abs_path, 0o700)
            file_match_and_replace_string(abs_path, "_OLK_PROPERTIES_TEMPLATE_OLK_RUN_HOME", OLK_RUN_HOME)


def load_properties(filepath, sep='=', comment_char='#'):
    """
    Read the file passed as parameter as a properties file.
    """
    props = {}
    with open(filepath, "rt") as f:
        for line in f:
            l = line.strip()
            if l and not l.startswith(comment_char):
                key_value = l.split(sep)
                key = key_value[0].strip()
                value = sep.join(key_value[1:]).strip().strip('"')
                props[key] = value
    return props


def get_cluster_name():
    seed_store_file = OLK_RUN_HOME + '/etc/seed-store.properties'
    seed_store_properties = load_properties(seed_store_file)
    name = seed_store_properties['seed-store.cluster']
    return name


def read_olkonyarn_deployment_properties():
    filename = OLK_RUN_HOME + '/olkonyarn_hdfs.properties'
    olkonyarn_hdfs_properties = load_properties(filename)
    return olkonyarn_hdfs_properties


def get_hdfs_client():
    deployment_params = read_olkonyarn_deployment_properties()

    if deployment_params['request-authentication'].lower() == 'insecure' or deployment_params['request-authentication'].lower() == 'ssl':
        return hdfs.InsecureClient(deployment_params['name-node-address'],
                                   user=deployment_params['hdfs-user'])
    elif deployment_params['request-authentication'].lower() == 'kerberos':
        import requests
        session = requests.Session()
        from hdfs.ext.kerberos import KerberosClient
        return KerberosClient(url=deployment_params['name-node-address'], session=session)

    print('--- Failed to retrieve hdfs client')
    exit(1)


def wk_seed_file_wait():
    if COORDINATOR:
        return

    hdfs_client = get_hdfs_client()
    retry_index = 0
    for retry_index in range(0, MAX_RETRY_COUNT):
        try:
            hdfs_client.status('/opt/hetu/seedstore/' + get_cluster_name() + '/seeds-resources.json')
            break
        except Exception as e:
            print(str(e))
            time.sleep(RETRY_INTERVAL)

    if retry_index == MAX_RETRY_COUNT - 1:
        sys.stderr.write('Max retry count reached!')
        exit(1)

    # Modify worker config.properties
    os.chmod(OLK_RUN_HOME + '/etc/config.properties', 0o700)

    with hdfs_client.read('/opt/hetu/seedstore/' + get_cluster_name() + '/seeds-resources.json') as reader:
        seeds_json = reader.read()

    discovery_uri = get_discovery_uri(seeds_json)

    last = len(discovery_uri.split(":")) - 1
    http_port = discovery_uri.split(":")[last]

    file_match_and_replace_string(OLK_RUN_HOME + '/etc/config.properties', '_OLK_PROPERTIES_TEMPLATE_DISCOVERY_URI',
                                  discovery_uri)
    file_match_and_replace_string(OLK_RUN_HOME + '/etc/config.properties',
                                  '_OLK_PROPERTIES_TEMPLATE_HTTP_SERVER_PORT',
                                  http_port)


def launch_openlookeng():
    subprocess.call(['/bin/bash', OLK_RUN_HOME + '/bin/launcher', 'run'])


if __name__ == '__main__':
    if OLK_RUN_HOME is None:
        sys.stderr.write("Failed to located run directory!")
        exit(1)

    if OLK_LIB_HOME is None:
        sys.stderr.write("Failed to located library directory!")
        exit(1)

    if COORDINATOR == 'true': 
        COORDINATOR = True
    else: 
        COORDINATOR = False

    create_olk_run_dir()
    copy_olk_instance_files()
    update_olk_run_files()
    wk_seed_file_wait()
    launch_openlookeng()
