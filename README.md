# openLooKeng-on-yarn

openLooKeng-on-YARN部署包提供了自动化脚本和配置模板以便在YARN上部署openLooKeng集群。

### 特性

1. 支持在单台机器上运行多个openLooKeng实例
2. 多集群部署
3. 能在一个集群中扩充/减少openLooKeng工作节点个数
4. 查询openLooKeng集群状态
5. 停止openLooKeng集群
6. 具有多个协调节点的高可用性部署，使用Nginx作为负载平衡器

### 前提条件

1. 一个带有YARN服务框架的Hadoop环境
    1. Apache Hadoop (经检测的版本: 3.1.4, 3.3.1)
    1. HDFS的用户权限
1. 环境要求
    1. Python3, 3.6及以上版本
        1. hdfs 2.6.0
        2. requests 2.22.0
        3. hdfs[kerberos] 2.6.0
        4. requests_kerberos 0.12.0

### 在YARN上的openLooKeng集群
./bin/olk_on_yarn.py脚本为在YARN上的openLooKeng集群提供了一站式的部署管理服务。

### 管理员配置

管理员通过编辑下列配置文件来设置正确的环境文件和集群配置。

#### HDFS 文件系统 - 必备

openLooKeng实例需要正确的配置来获取HDFS的数据。管理员将被要求提供下列文件的正确配置。这些文件在该目录下： **conf/etc_template/filesystem/**。

1. core-site.xml
2. hdfs-site.xml

#### 部署

通过部署自动化脚本，如下参数可被提供：
1. openLooKeng集群名称和服务端口号
2. 工作节点和协调节点的数量
3. 启用/禁用高可用性
4. YARN资源管理器的socket地址

自动化脚本的配置样例
- olk_on_yarn_default.json (默认)

以下目录为openLooKeng协调节点提供配置文件:

- conf/etc_template/coordinator

以下目录为openLooKeng工作节点提供配置文件:

- conf/etc_template/worker

#### 插件 & 数据源

关于openLooKeng的其他配置文件均在协调节点和工作节点的文件夹下。

#### 衍生工作

bin/launcher.py的原始版本可在以下目录获取：
https://github.com/airlift/airlift/blob/master/launcher/src/main/scripts/bin/launcher.py
